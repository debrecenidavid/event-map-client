const path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
    mode: 'development',
    entry: {
        app: ['./src/app/index.tsx', 'webpack-hot-middleware/client'],
        admin: ['./src/admin/admin.tsx', 'webpack-hot-middleware/client'],
        vendor: ['react', 'react-dom']
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].bundle.js'
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx', '.css']
    },
    module: {
        rules: [
            { test: /\.(ts|tsx)$/, loader: 'ts-loader' },
            {test: /\.png$/, loader: 'url-loader'},
            { test: /\.less$/, use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'less-loader' }] },
            { test: /\.js$/, enforce: "pre", loader: "source-map-loader" }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({favicon: 'src/app/favicon.ico',chunks:['app','vendor'], template: path.resolve(__dirname, 'src', 'app', 'index.html') }),
        new HtmlWebpackPlugin({favicon: 'src/app/favicon.ico',filename:'admin.html',chunks:['admin','vendor'], template: path.resolve(__dirname, 'src', 'admin', 'admin.html') }),
        new webpack.HotModuleReplacementPlugin() 
    ]
};

module.exports = config;