import * as React from 'react';
import { Store, User, } from "../types";
import { connect } from 'react-redux';
import { mapDispatchToProps,makeRequest } from '../utils';
import * as _ from 'lodash';

class Login extends React.Component<any, {}> {
    render() {
        const {setData,errors,loginName,pw} = this.props;

        const onClick=()=>{
           let body=JSON.stringify({email:loginName,password:pw})
            makeRequest("POST","login","",body).then(e=>{
                if(e){
                    if(e.success){
                        setData("isLoggedIn",true);
                        window.localStorage.setItem("jwtToken", e.token);
                        setData("pw",undefined)
                        setData("errors",undefined);
                    }else{
                        setData("errors",e.errors);
                    }
                }else{
                    this.props.setData("isLoggedIn",false)
                }
            }).catch(err=>console.log("err"));
            
        }
        const handlepwChange=(e)=>{
            const { value } = e.target;
            setData("pw",value)
        }
        const handleEmailChange=(e)=>{
            const { value } = e.target;
            setData("loginName",value)
        }
        let errorsDivs;
        if(errors){
            errorsDivs=errors.map(error=>{return <div key={error.message} className="error-message">{error.message}</div>})
        }

        return (
            <div className="Login">
            <input type="text" name="email" value={loginName} onChange={handleEmailChange} />
            <input type="password" name="password" value={pw} onChange={handlepwChange} />
            <button onClick={onClick}>
            Login
            </button>
            <br/>
            {errorsDivs}
            </div>
        );}}


function mapStateToProps(store: Store) {
    return {
        errors:store.errors,
        loginName:store.loginName,
        pw:store.pw
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);