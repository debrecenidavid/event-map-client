import * as React from 'react';
import { Store, User,Storage } from "../types";
import { connect } from 'react-redux';
import { mapDispatchToProps,makeRequest } from '../utils';
import * as _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import categorys from '../categoryFilters'
import city from '../cityFilters'

class AdminPage extends React.Component<any, {}> {
    componentDidMount() {
        if(!this.props.user){
        makeRequest("GET","current",window.localStorage['jwtToken']).
        then(e=>{
            this.props.setData("user",e)
        }).catch(err=>{
            console.log(err)
        })
    }
    }
     componentDidUpdate(){
         if(!this.props.user){
             makeRequest("GET","current",window.localStorage['jwtToken']).
             then(e=>{
                 this.props.setData("user",e)
                }).catch(err=>{
                    console.log(err)
                })
            }
     }
    render() {
        const isDevice = window['isDevice'];
        const { user,setData,errors,pages,pageToSave,searchCity} = this.props;
        let errorsDivs;
        if(errors){
            errorsDivs=errors.map(error=>{return <div key={error.message} className="error-message">{error.message}</div>})
        }
        
        const handleCategoryChange1=(e)=>{
            const { value } = e.target;
            setData("pageToSave.tag1",value);
        }
        const handleCategoryChange2=(e)=>{
            const { value } = e.target;
            setData("pageToSave.tag2",value);
        }
        const handleCategoryChange3=(e)=>{
            const { value } = e.target;
            setData("pageToSave.tag3",value);
        }
        const handleCityChange=(e)=>{
            const { value } = e.target;
            setData("pageToSave.city",value);
        }
        const handleFacebookPageIdChange=(e)=>{
            const { value } = e.target;
            setData("pageToSave.facebookPageId",value);
        }
        const handleSearchCityChange=(e)=>{
            const { value } = e.target;
            setData("searchCity",value);
        }
        const addPage=()=>{
            if(pageToSave.facebookPageId && pageToSave.city && pageToSave.tag1||pageToSave.tag2||pageToSave.tag3){
                setData('errors',undefined)
                let body=JSON.stringify({
                    facebookPageId:pageToSave.facebookPageId,
                    tag1:pageToSave.tag1,
                    tag2:pageToSave.tag2,
                    tag3:pageToSave.tag3,
                    city:pageToSave.city})
                    makeRequest("POST","savePageToDb",window.localStorage['jwtToken'],body).then(e=>{
                    if(e){
                        if(e.success){
                            // setData("isLoggedIn",true);
                            setData('errors',[{message:"Succes"}])
                        }else{
                            // setData("isLoggedIn",false);
                            setData("errors",[{message:e.errors.sqlMessage}]);
                        }
                    }else{
                        setData("isLoggedIn",false)
                    }

                    }).catch(err=>console.log("err"));
                }else{
                    setData('errors',[{message:"FacebookPageId, City and min. 1 tag required"}])
                }
            }
            let categoryOptions=categorys.map(c=>{return <option key={c.value} value={c.value}>{c.name}</option>})
            let cityOptions=city.map(c=>{return <option key={c.value} value={c.value}>{c.name}</option>})
            const getPages=()=>{
                let body=JSON.stringify({
                    city:searchCity})
                makeRequest("POST","pages",window.localStorage['jwtToken'],body).
                then(e=>{
                this.props.setData("pages",e.pages);
                })
            }

            if(user){
                   let pagesDiv=
                    <BootstrapTable width='700' data={ pages }>
                    <TableHeaderColumn width='50' dataField='id' >ID</TableHeaderColumn>
                    <TableHeaderColumn width='300' dataField='facebookPageId' isKey>Name</TableHeaderColumn>
                    <TableHeaderColumn width='230' dataField='location'>Location</TableHeaderColumn>
                    <TableHeaderColumn width='150' dataField='City'>City</TableHeaderColumn>
                    <TableHeaderColumn width='350' dataField='lastFetched'>Last Fetched</TableHeaderColumn>
                    <TableHeaderColumn width='150' dataField='tags'>Tag</TableHeaderColumn>
                    </BootstrapTable>
                return (
            <div className='adminPage'>
                <div>Hello {user.name}</div>
                <button onClick={()=>{setData("isLoggedIn",false);window.localStorage.clear()}}>
                Logout
                </button>
                <div className="AdminPage">
                FacebookPageId: <input type="text" name="facebookPageId" value={pageToSave.facebookPageId} onChange={handleFacebookPageIdChange}/>
                City: <select value={pageToSave.city} onChange={handleCityChange}> 
                    {cityOptions}
                </select>
                Tag 1: <select value={pageToSave.tag1} onChange={handleCategoryChange1}> 
                    {categoryOptions}
                </select>
                Tag 2: <select value={pageToSave.tag2} onChange={handleCategoryChange2}>
                    {categoryOptions}
                </select >
                Tag 3: <select value={pageToSave.tag3} onChange={handleCategoryChange3}>
                    {categoryOptions}
                </select>
                 <button onClick={addPage}>
                    Add
                </button>
                 <br/>
                {errorsDivs}
                <div>
                City: <select value={searchCity} onChange={handleSearchCityChange}> 
                    {cityOptions}
                </select>
                 <button onClick={getPages}>
                    GetPages
                </button>
                </div>
                 {pagesDiv}
                </div>
            </div>
            )
        }else{
            return <div>Loading...</div>
        }
        ;}}


function mapStateToProps(store: Store) {
    return {
        user: store.user,
        errors:store.errors,
        pages:store.pages,
        pageToSave:store.pageToSave,
        searchCity:store.searchCity
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);