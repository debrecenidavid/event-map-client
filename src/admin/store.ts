import { createStore } from 'redux'
import { appReducer } from './reducers';
declare let module: any

const store = createStore(appReducer);

if (module.hot) {
    module.hot.accept();
}
window['store'] = store;
export default store;