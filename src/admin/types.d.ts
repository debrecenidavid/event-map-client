export type Store = {
    isLoggedIn: boolean,
    user?: User,
    jwtToken:String,
    errors:any
    loginName:String,
    pw:String,
    pages:any[]
    pageToSave:any,
    pagesCalled:boolean,
    userRequested:boolean,
    searchCity:any
};
export type Storage={
    jwtToken:String
}
export type User={
    id:String,
    name:String,
    email:String
}
export type Email=any