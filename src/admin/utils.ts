import { Store } from "./types";
import { setData } from './actions';

const isLocal = location.hostname === 'localhost';
const apiEnd = isLocal ? 'http://localhost:8008/admin/' : 'http://18.197.214.54:8008/admin/';

export function makeRequest(type,route,token,args?: any) {
    if(type==="GET"){
        const returnPromise = fetch(apiEnd+route, {
            method: `${type}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization':`${token}`
            },
            // body: JSON.stringify({
            //     args
            // }),
        }).then(res => {
            // console.log(res)
            if(res.status===401){
                return undefined;
            }else{
                return res.json();
            }
        }).catch(err => {
            console.error(`Error with data request [route: ${route}]`, args, err);
        });
        return returnPromise;
    }else{
        const returnPromise = fetch(apiEnd+route, {
            method: `${type}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization':`${token}`
            },
            body: 
                args
            
        }).then(res => {
            // console.log(res)
            if(res.status===401){
                return undefined;
            }else{
                return res.json();
            }
        }).catch(err => {
            console.error(`Error with data request [route: ${route}]`, args, err);
        });
        return returnPromise;
    }
}

export function mapStateToProps(store: Store) {
    return store;
}

export function mapDispatchToProps(dispatch) {
    return {
        setData: (path, value) => dispatch(setData(path, value)),
    };
}