import * as _ from 'lodash';
import { Store } from './types';

const initialAppState: Store = {
        isLoggedIn: false,
        user:undefined,
        jwtToken:undefined,
        errors:undefined,
        loginName:undefined,
        pw:undefined,
        pages:undefined,
        pagesCalled:false,
        userRequested:false,
        searchCity:"",
        pageToSave:{
            facebookPageId:"",
            tag1:"",
            tag2:"",
            tag3:"",
            city:""

        }
};

const isLocal = location.hostname === 'localhost';
export const appReducer = function (state = initialAppState, action) {
    switch (action.type) {
        case 'setData':
        const { path, value } = action.payload;
        const stateCopy = _.cloneDeep(state);
        _.set(stateCopy, path, value);
            if(isLocal) {
                console.log('[setData]', path, value);
                console.log('[newState]', stateCopy);
            }
            return stateCopy;
        default:
            return state;
    }
}