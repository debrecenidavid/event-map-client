
import * as React from 'react';
import { connect } from 'react-redux';
import { makeRequest, mapStateToProps, mapDispatchToProps} from './utils';
import { Store } from './types';
import Login from './components/login'
import Adminpage from './components/adminpage'

type AppProps = Store & {
    setData: (path: string, value: any) => void,
}

class App extends React.Component<AppProps, {}> {
    componentWillMount() {
           const a= makeRequest("GET","current",window.localStorage['jwtToken']).
           then(e=>{
            console.log(e)
            if(e){
                this.props.setData("isLoggedIn",true)
            }else{
                this.props.setData("isLoggedIn",false)
            }
        }).catch(err=>{
         console.log("error")
            console.log(err)
        })
        }

    componentDidUpdate(){
        // if(this.props.config.cityChanged){
        //     this.props.setData('config.cityChanged',false)
        //     makeRequest('events',this.props.config.bounds).then(res => {
        //         this.props.setData('data.events', res); 
        //     })
        // }
    }

    render() {
        const { isLoggedIn } = this.props;
        if (!isLoggedIn) {
            return (
                <Login/>
            );
        }else{
            const isDevice = window['isDevice'];
            return (
                <Adminpage/>
            );
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);