export const setData = (path, value) => {
    return { type: 'setData', payload: { path, value } }
}

export function errorAfterFiveSeconds() {
    return (dispatch) => {
        setTimeout(() => {
            dispatch(setData('config.isLoading', false));
        }, 5000);
    };
}