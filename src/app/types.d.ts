import categoryFilters from "./filters/categoryFilters";

export type Store = {
    config: AppConfig,
    data: AppData,
};

export type AppConfig = {
    user: {
        location?: LocationObj,
    },
    isLoading: boolean,
    dateFilterOpen: boolean,
    categoryFilterOpen: boolean,
    cityFilterOpen: boolean,
    dateTimeSliderOpen: boolean,
    contactPageOpen: boolean,
    aboutPageOpen: boolean,
    cityChanged: boolean,
    filters: EventFilters,
    position: any,
    bounds: any,
    email: String,
    feedbackText: String,
    emailSaveSuccess: boolean,
    feedbackSaveSuccess: boolean,
    emailError:boolean,
    feedbackError:boolean,

}

export type LocationObj = {
    lat: number,
    lng: number,
};

export type DateFilter = {
    name: string,
    startTs: number,
    endTs: number,
}
export type EventFilters = {
    startTs?: number,
    endTs?: number,
    tags?: any,
    city?: any
    dateSlideStartTs: any
    slider: number
}

export type AppData = {
    dateFilters?: DateFilter[],
    categoryFilters?: String[],
    cityFilters?: any,
    lastFetched?: number,
    locations?: Location[],
    tags?: any,
    events?: Event[],
};

type TagId = string;
export type Tag = {
    id: TagId,
    name: string,
    icon?: string,
};

export type Location = {
    id: number,
    name: string,
    lat: number,
    lng: number,
};

export type Event = {
    id: number,
    facebookId: string,
    name: string,
    startTs: number,
    endTs: number,
    guests: number,
    location: Location,
    tags: Tag[],
    link: string,
};