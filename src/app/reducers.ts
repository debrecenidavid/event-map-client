import * as _ from 'lodash';
import * as moment from 'moment';
import { Store } from './types';

const todayStartOffset = 6 * 3600000; // 6 hours
const now = moment().startOf('day').valueOf() + todayStartOffset;
const initialAppState: Store = {
    config: {
        isLoading: true,
        dateFilterOpen: false,
        categoryFilterOpen:false,
        cityFilterOpen:false,
        cityChanged:false,
        dateTimeSliderOpen:true,
        contactPageOpen:false,
        aboutPageOpen:false,
        filters: {
            startTs: moment().valueOf()>now ? now:moment().startOf('hour').valueOf(),
            endTs: moment().endOf('day').valueOf() + todayStartOffset,
            tags:"All Category",
            city:"Debrecen",
            dateSlideStartTs:moment().valueOf()>now ? now:moment().startOf('hour').valueOf(),
            slider:0
        },
        email:'',
        emailError:false,
        feedbackError:false,
        feedbackText:'',
        emailSaveSuccess:false,
        feedbackSaveSuccess:false,
        position:[47.53184129, 21.62922478],
        // bounds: [[47.42, 21.8], [47.62, 21.42]],
        bounds: [[47.42, 21.42], [47.62, 21.8]],//left bottom, right top
        user: {}

    },
    data: {},
};

const isLocal = location.hostname === 'localhost';
export const appReducer = function (state = initialAppState, action) {
    switch (action.type) {
        case 'setData':
        const { path, value } = action.payload;
        const stateCopy = _.cloneDeep(state);
        _.set(stateCopy, path, value);
            if(isLocal) {
                console.log('[setData]', path, value);
                console.log('[newState]', stateCopy);
            }
            return stateCopy;
        default:
            return state;
    }
}