import { Store } from "./types";
import { setData } from './actions';

const isLocal = location.hostname === 'localhost';
const apiEnd = isLocal ? 'http://localhost:8008/' : 'http://18.197.214.54:8008/';

export function makeRequest(action, args?: any) {
    const returnPromise = fetch(apiEnd, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            action,
            args
        }),
    }).then(res => {
        return res.json();
    }).catch(err => {
        console.error(`Error with data request [action: ${action}]`, args, err);
    });
    return returnPromise;
}

export function mapStateToProps(store: Store) {
    return store;
}

export function mapDispatchToProps(dispatch) {
    return {
        setData: (path, value) => dispatch(setData(path, value)),
    };
}

export function getEventsForDates(events, startTs, endTs) {
    return events.filter(event => event.startTs > startTs && event.startTs < endTs || event.endTs > startTs && event.endTs < endTs) || [];
}

export function getEventsForTime(events, timeTs) {
    return events.filter(event => event.startTs <= timeTs && event.endTs > timeTs) || [];
}

export function getEventsForTags(events, tag) {
    return events.filter(event => event.tags.indexOf(tag) !== -1) || [];
}

export function getIconUrl(name) {
    switch (name) {
        case "Bar":
        return  require('./markers/Bar.png')
       case "City":
        return require('./markers/City.png')
       case "Club":
        return require('./markers/Club.png')
       case "Concert":
        return require('./markers/Concert.png')
       case "Culture":
        return require('./markers/Culture.png')
       case "Dining":
        return require('./markers/Dining.png')
       case "Education":
        return require('./markers/Education.png')
       case "Food":
        return require('./markers/Food.png')
       case "Music":
        return require('./markers/Music.png')
       case "Pub":
        return require('./markers/Pub.png')
       case "Sport":
        return require('./markers/Sport.png')
       case "Youth":
        return require('./markers/Youth.png')
        default:
            return require('./markers/Youth.png');
    }
}


export function getLocation() {
    return new Promise((resolve, reject) => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                resolve({ lat: position.coords.latitude, lng: position.coords.longitude })
            }, (err) => reject(err));
        } else {
            reject('Location not supported.');
        }
    })
}