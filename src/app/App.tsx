
import * as React from 'react';
import { connect } from 'react-redux';
import { makeRequest, mapStateToProps, mapDispatchToProps, getLocation } from './utils';
import EventMap from './components/EventMap';
import { Store } from './types';
import Loading from './components/Loading';
import TopNavBar from './components/TopNavBar'
import dateFilters from './filters/dateFilters';
import categoryFilters from './filters/categoryFilters';
import cityFilters from './filters/cityFilters';

type AppProps = Store & {
    setData: (path: string, value: any) => void,
}

class App extends React.Component<AppProps, {}> {
    componentWillMount() {
        const setData = this.props.setData;
        makeRequest('events',this.props.config.bounds).then(res => {
            setData('data.events', res); 
            setData('data.dateFilters',dateFilters)
            setData('data.categoryFilters',categoryFilters)
            setData('data.cityFilters',cityFilters)
        }
        );
        // TODO: set an interval for updateing geolocation
        getLocation().then(location => setData('config.user.location', location));

    }

    componentDidUpdate(){
        if(this.props.config.cityChanged){
            this.props.setData('config.cityChanged',false)
            makeRequest('events',this.props.config.bounds).then(res => {
                this.props.setData('data.events', res); 
            })
        }
    }

    render() {


        const { config, data } = this.props;
        const hasAppDataLoaded = !!data.events;
        if (!hasAppDataLoaded) {
            return (
                <Loading />
            );
        }
        const isDevice = window['isDevice'];
        return (
            <div id="appContainer" className={isDevice ? 'mobile' : ''}>
                {/* <TopNavBar/> */}
                <EventMap />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);