import * as React from 'react';
import { connect } from 'react-redux';
import { mapDispatchToProps, makeRequest} from '../utils';
import {Store} from '../types';
import FontAwesome from 'react-fontawesome'

class ContactPage extends React.Component<any, {}> {
    render() {
        const {config,setData} = this.props;
        const { contactPageOpen,feedbackText,feedbackSaveSuccess,feedbackError} = config;
        const filterState = contactPageOpen ? 'open' : 'closed';
        const saveFeedback=()=>{
            if(feedbackText.length===0){
                setData("config.feedbackError",true)
            }else{
                setData("config.feedbackError",false)
                makeRequest('saveFeedback',feedbackText).then(res => {
                    setData("config.feedbackSaveSuccess",true)
                });
            }
        }
        const handleFeedbackChange=(e)=>{
            const { value } = e.target;
            setData("config.feedbackText",value)
        }
        let errorContent=feedbackError?<span style={{color:"red"}}>A leírás megadása kötelező!</span>:""
        let feedbackContent=feedbackSaveSuccess?
                    <h5>
                      <br/>
                      <span style={{color:"#4caf50"}}>Köszönjuk a visszajelzést!</span>
                      <span> </span>
                    </h5>:
                    <span>
                    <textarea value={feedbackText} onChange={handleFeedbackChange} placeholder='Javaslatod, kérdésed van vagy hibát találtál? Írj nekünk!' className='contactTextArea'>
                    </textarea>
                    <br/>
                    {errorContent}
                    <h3>
                    <FontAwesome name='share-square' onClick={saveFeedback}/>
                    </h3>
                    </span>
        return (
            <div className={`contactPage ${filterState}`}>
            <div className="toggleIcon" onClick={() => {
                    setData('config.contactPageOpen', !contactPageOpen);
                }}>
                    <FontAwesome name='close' />
                </div>
            <img src={require('../logo.png')} width="100" height="108"/>
            <h3>
            <a className='linkClass' href="mailto:todohungary@gmail.com">
            <FontAwesome name='envelope'/>
                <span> </span>
                todohungary</a>
            </h3>
            <h3>
            <a className='linkClass' href="//www.facebook.com/todohungary/">
            <FontAwesome name='facebook-square'/>
            <span> </span>
                //TODOHungary</a>
            </h3>
            {feedbackContent}
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        config: store.config,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactPage);