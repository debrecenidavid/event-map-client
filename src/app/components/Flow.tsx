const questions = [
    {
        id: 'group',
        name: 'Hányan vagytok?',
        options: [
            {
                id: 'alone',
                name: 'Egyedül mennék',
            },
            {
                id: 'couple',
                name: 'Párommal',
            },
            {
                id: 'small',
                name: '2 - 3 Fö',
            },
            {
                id: 'large',
                name: 'Több mint 4 fö',
            },
        ],
    },
    {
        id: 'age',
        name: 'Korosztály?',
        options: [
            {
                id: 'child',
                name: 'Gyerek',
            },
            {
                id: 'young',
                name: 'Fiatal',
            },
            {
                id: 'retro',
                name: 'Retro',
            },
            {
                id: 'any',
                name: 'Teljesen mindegy',
            },
        ],
    },
    {
        id: 'when',
        name: 'Mikor?',
        options: [
            {
                id: 'today',
                name: 'Ma',
            },
            {
                id: 'tomorrow',
                name: 'Holnap',
            },
            {
                id: 'week',
                name: 'A héten',
            },
            {
                id: 'weekend',
                name: 'A hétvégén',
            },
            {
                id: 'any',
                name: 'Valamikor...',
            },
        ],
    },
    {
        id: 'length',
        name: 'Meddig mennétek?',
        options: [
            {
                id: 'short',
                name: '1 - 2 órára',
            },
            {
                id: 'afternoon',
                name: 'Delutan',
            },
            {
                id: 'night',
                name: 'Éjszakai',
            },
        ],
    },
    {
        id: 'budget',
        name: 'Mennyit akarsz költeni?',
        options: [
            {
                id: 'free',
                name: 'Ingyen!!!',
            },
            {
                id: 'thousand',
                name: `Páreze'`,
            },
            {
                id: 'tenthousand',
                name: 'Egy ticura gondoltam',
            },
            {
                id: 'any',
                name: 'Nem számít a pénz!',
            },
        ],
    },
];