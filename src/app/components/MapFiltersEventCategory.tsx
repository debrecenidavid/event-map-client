import * as React from 'react';
import { connect } from 'react-redux';
import * as FontAwesome from 'react-fontawesome';
import { mapDispatchToProps, getEventsForTags, getEventsForDates } from '../utils';
import { Store, EventFilters, DateFilter, Event } from '../types';
import * as _ from 'lodash';

class MapFiltersEventCategory extends React.Component<{cityFilterOpen:boolean, categoryFilterOpen: boolean,dateFilterOpen:boolean,categoryFilters:any,filters: EventFilters, events: Event[], setData: any }, {}> {
    render() {
        const isDevice = window['isDevice'];
        const { categoryFilterOpen,cityFilterOpen, dateFilterOpen,events, setData,filters,categoryFilters } = this.props;
        let filterContents;
        if (categoryFilterOpen) {
            const { tags,startTs, endTs } = filters;
            function makeDateFilterButton(name, isDisabled, buttonTag) {
                const isActive = tags === buttonTag ;
                const onClick = isDisabled ? undefined : () => {
                    if (isDevice) {
                        setData('config.categoryFilterOpen', false)
                    }
                    if(tags !== buttonTag){
                        setData('config.filters.tags', buttonTag);
                    }
                };
                if(isDisabled) return (<div key={`${buttonTag}`}></div>)
                return (
                    <button
                    key={`${buttonTag}`}
                    className={isActive ? 'button-primary' : isDisabled ? 'disabled' : ''}
                    onClick={onClick}>{name}
                 </button>
                )
            }
            
            const eventForDate=getEventsForDates(events || [], startTs, endTs)
            const uniqOptions = _.uniqBy(categoryFilters, item => `${item.name}`)
                .map(opt => {
                    let hasEvents;
                    if(opt.name==='All Category'){
                        hasEvents=1
                    }else{
                         hasEvents = getEventsForTags(eventForDate || [], opt.name).length;
                    }
                    return Object.assign(opt, { hasEvents })
                });
            filterContents = (
                <div className="controller">
                    {/* <h6 className="title">Category</h6> */}
                    <img src={require('../logo.png')} width="100" height="108"/>
                    <div className="vertical">
                        {uniqOptions.map(t => makeDateFilterButton(t.name, !t.hasEvents,t.name))}
                    </div>
                    <pre>
                        <code>
                            Credits: <a href="mailto:todohungary@gmail.com">//TODOHungary</a>
                        </code>
                    </pre>
                </div>
            )
        }
        let logo=require('../logo.png')
        const filterState = categoryFilterOpen ? 'open' : 'closed';
        const filterContainer = categoryFilterOpen
            ? (<div>
                <div className="toggleIcon" onClick={() => {
                    setData('config.categoryFilterOpen', !categoryFilterOpen);
                }}>
                    <FontAwesome name='close' />
                </div>
                {filterContents}
            </div>)
            : (<div className="toggleIcon" onClick={() => {
                setData('config.categoryFilterOpen', !categoryFilterOpen);
                setData('config.dateFilterOpen',false);
                setData('config.cityFilterOpen',false);
                setData('config.aboutPageOpen',false);
                setData('config.contactPageOpen',false);
            }}>
                <FontAwesome name='sliders' />
            </div>);
            const categoryIcon=categoryFilterOpen && !isDevice?
            (<div className="categoryIcon" onClick={() => {
             setData('config.categoryFilterOpen', !categoryFilterOpen);
             setData('config.dateFilterOpen',false);
             setData('config.cityFilterOpen',false);
             setData('config.aboutPageOpen',false);
             setData('config.contactPageOpen',false);
             }}>
             <FontAwesome name='sliders'/>
             </div>):(<div></div>)
        if(dateFilterOpen && isDevice || cityFilterOpen && isDevice) return(<div></div>)
        return (
            <div onClick={() => {
                isDevice?
                setData('config.categoryFilterOpen', !categoryFilterOpen):"";
            }}>
            {categoryIcon}
            <div className={`mapControlCategory ${filterState}`}>
                {filterContainer}
            </div>
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        categoryFilterOpen: store.config.categoryFilterOpen,
        dateFilterOpen:store.config.dateFilterOpen,
        cityFilterOpen:store.config.cityFilterOpen,
        tags: store.config.filters.tags,
        filters: store.config.filters,
        categoryFilters: store.data.categoryFilters,
        events: store.data.events,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapFiltersEventCategory);