import * as React from 'react';
import { connect } from 'react-redux';
import * as FontAwesome from 'react-fontawesome';
import { mapDispatchToProps, getEventsForTags, getEventsForDates } from '../utils';
import { Store, EventFilters, DateFilter, Event } from '../types';
import * as _ from 'lodash';

class MapFiltersEventCity extends React.Component<{ categoryFilterOpen: boolean,cityFilterOpen:boolean,cityFilters:any, dateFilterOpen:boolean,filters: EventFilters, events: Event[], setData: any }, {}> {
    render() {
        const isDevice = window['isDevice'];
        const { categoryFilterOpen, dateFilterOpen,cityFilterOpen,events, setData,filters,cityFilters } = this.props;
        let filterContents;
        if (cityFilterOpen) {
            const { tags,startTs, endTs, city} = filters;
            function makeDateFilterButton(name, isDisabled, buttonCity) {
                const isActive = city === buttonCity.name ;
                const onClick = isDisabled ? undefined : () => {
                    if (isDevice) {
                        setData('config.cityFilterOpen', false)
                    }
                    if(city !== buttonCity.name){
                        setData('config.position', buttonCity.position);
                        setData('config.bounds', buttonCity.bounds);
                        setData('config.filters.city',name);
                        setData('config.cityChanged',true);
                    }
                };
                if(isDisabled) return (<div key={`${buttonCity.name}`}></div>)
                return (
                    <button
                    key={`${buttonCity.name}`}
                    className={isActive ? 'button-primary' : isDisabled ? 'disabled' : ''}
                    onClick={onClick}>{name}
                 </button>
                )
            }
            
            filterContents = (
                <div className="controller">
                    {/* <h6 className="title">City</h6> */}
                    <img src={require('../logo.png')} width="100" height="108"/>
                    <div className="vertical">
                        {cityFilters.map(t => makeDateFilterButton(t.name, false,t))}
                    </div>
                    <pre>
                        <code>
                            Credits: <a href="mailto:todohungary@gmail.com">//TODOHungary</a>
                        </code>
                    </pre>
                </div>
            )
        }
        const filterState = cityFilterOpen ? 'open' : 'closed';
        const filterContainer = cityFilterOpen
            ? (<div>
                <div className="toggleIcon" onClick={() => {
                    setData('config.cityFilterOpen', !cityFilterOpen);
                }}>
                    <FontAwesome name='close' />
                </div>
                {filterContents}
            </div>)
            : (<div className="toggleIcon" onClick={() => {
                setData('config.cityFilterOpen', !cityFilterOpen);
                setData('config.dateFilterOpen',false);
                setData('config.categoryFilterOpen',false);
                setData('config.aboutPageOpen',false);
                setData('config.contactPageOpen',false);
            }}>
                <FontAwesome name='university' />
            </div>);
            const cityIcon=cityFilterOpen && !isDevice?
            (<div className="cityIcon" onClick={() => {
             setData('config.cityFilterOpen', !cityFilterOpen);
             setData('config.dateFilterOpen',false);
             setData('config.categoryFilterOpen',false);
             setData('config.aboutPageOpen',false);
             setData('config.contactPageOpen',false);
             }}>
             <FontAwesome name='university'/>
             </div>):(<div></div>)
        if(dateFilterOpen && isDevice || categoryFilterOpen && isDevice) return(<div></div>)
        return (
            <div onClick={() => {
                isDevice?
                setData('config.cityFilterOpen', !cityFilterOpen):"";
            }}>
            {cityIcon}
            <div className={`mapControlCity ${filterState}`}>
                {filterContainer}
            </div>
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        categoryFilterOpen: store.config.categoryFilterOpen,
        dateFilterOpen:store.config.dateFilterOpen,
        cityFilterOpen:store.config.cityFilterOpen,
        tags: store.config.filters.tags,
        filters: store.config.filters,
        cityFilters: store.data.cityFilters,
        events: store.data.events,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapFiltersEventCity);