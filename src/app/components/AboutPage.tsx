import * as React from 'react';
import { connect } from 'react-redux';
import { mapDispatchToProps,makeRequest} from '../utils';
import {Store} from '../types';
import FontAwesome from 'react-fontawesome'

class AboutPage extends React.Component<any, {}> {
    render() {
        const {config,setData,} = this.props;
        const { aboutPageOpen,email,emailSaveSuccess,emailError} = config;
        const filterState = aboutPageOpen ? 'open' : 'closed';
        const validateEmail=(email)=>{
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
        }
        const saveEmail=()=>{
            if(!validateEmail(email)){
                setData("config.emailError",true)
            }else{
                setData("config.emailError",false)
                makeRequest('saveEmail',email).then(res => {
                    setData("config.emailSaveSuccess",true)
                });
            }
        }
        const handleEmailChange=(e)=>{
            const { value } = e.target;
            setData("config.email",value)
        }
        let errorContent=emailError?<span style={{color:"red"}}>Nem megfelelő email cim</span>:"";
        let emailContent=emailSaveSuccess?
                    <h5>
                      <span style={{color:"#4caf50"}}>Email cím sikeresen elmentve!</span>
                      <span> </span>
                    </h5>:
                    <h5>
                      <input placeholder='email' value={email} onChange={handleEmailChange} type='text' />
                      <span> </span>
                      <FontAwesome name='share-square' onClick={saveEmail}/>
                      <br/>
                      {errorContent}
                    </h5>
        return (
            <div className={`aboutPage ${filterState}`}>
            <div className="toggleIcon" onClick={() => {
                    setData('config.aboutPageOpen', !aboutPageOpen);
                }}>
                    <FontAwesome name='close' />
                </div>
                <img src={require('../logo.png')} width="100" height="108"/>
                <h3>Fő célunk, küldetésünk</h3>
                <ul>
                    <li>Megreformálni és fellendíteni a lokális közösségi életet</li>
                    <li>Gyors és áttekinthető platformot létrehozni</li>
                    <li>Eseményeket egy helyen összegyűjteni</li>
                </ul>
                <span>A platform jellenleg csak webes felületen érhető el,
                     a mobil alkalmazás jellenleg <br/> fejlesztés és tesztelés alatt van.
                     Ha szeretnél az elsők között értesülni a legújabb <br/>  funkciókról
                      illetve a mobil alkalmazásról, 
                      akkor add meg az email cimed <br/> 
                       valamint kövess minket a Facebookon.
                      <br/>
                      <br/>
                      {/* <h5> */}
                      {emailContent}
                      {/* <input placeholder='email' value={email} onChange={handleEmailChange} type='text' />
                      <span> </span>
                      <FontAwesome name='share-square' onClick={saveEmail}/>
                      </h5> */}
                      <h5>
                      <a className='linkClass' href="//www.facebook.com/todohungary/">
                        <FontAwesome name='facebook-square'/>
                        <span> </span>
                    //TODOHungary</a>
                     </h5>
                    </span>
                    <h5>Te se maradj le semmiről!</h5>
                    {/* <h6>&copy; Copyright 2018 //TODOHungary</h6> */}
                    <footer className='copyrightFooter'>&copy; Copyright 2018 //TODOHungary</footer>
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        config: store.config,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutPage);