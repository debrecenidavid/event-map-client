import * as React from 'react';
import { connect } from 'react-redux';
import { Map, Marker, CircleMarker, Popup, TileLayer } from 'react-leaflet'
import * as moment from 'moment';
import { mapDispatchToProps, getEventsForDates,getEventsForTags,getIconUrl,getEventsForTime } from '../utils';
import { Event, Store, DateFilter, LocationObj,EventFilters } from '../types';
import MapFilters from './MapFilters';
import MapFiltersEventCategory from './MapFiltersEventCategory'
import MapFiltersEventCity from './MapFiltersEventCity'
import DateTimeSlider from './DateTimeSlider'
import L from "leaflet";
import * as _ from 'lodash';
import ContactPage from './ContactPage'
import AboutPage from './AboutPage'
import TopNavBar from './TopNavBar'

class EventMap extends React.Component<{
    position:any,
    bounds:any, 
    events: Event[],
    setData: any,
    dateFilters: DateFilter[],
    userLocation: LocationObj,
    filters: EventFilters
}, {}> {
    render() {

        const {position,bounds, events, filters, setData, dateFilters, userLocation } = this.props;
        const { tags,startTs, endTs,city,dateSlideStartTs,slider } = filters;
        let filteredEvents = [];
        let locationMarkers;
        if (events.length>0) {
            let eventsForDate;
            if(startTs>=dateSlideStartTs && slider===0){
                eventsForDate = getEventsForDates(events, startTs, endTs);
            }else if(startTs<dateSlideStartTs && endTs-startTs<31 * 3600000){
                eventsForDate = getEventsForTime(events,dateSlideStartTs)
            }
            else{
                eventsForDate = getEventsForDates(events, dateSlideStartTs, dateSlideStartTs+24*3600000);
            }
            if(tags!=='All Category'){
                eventsForDate=getEventsForTags(eventsForDate,tags);
            }
            // if (!eventsForDate.length && startTs>=dateSlideStartTs) {
            //     console.log("asdasdasdasd"+events.length)
            //     const uniqOptions = _.uniqBy(dateFilters, item => `${item.startTs}-${item.endTs}`);
            //     const dateFilterWithEvents = uniqOptions.find(opt => getEventsForDates(events, opt.startTs, opt.endTs).length)
            //     setData('config.filters.startTs', dateFilterWithEvents.startTs);
            //     setData('config.filters.endTs', dateFilterWithEvents.endTs);
            // } else {
                filteredEvents=eventsForDate;
                // }
            // }

        //TODO loc may hide each other
        let locationNames=[]
        filteredEvents.sort((a, b) => a.startTs > b.startTs ? 1 : -1);
        let locationsEvents=filteredEvents.reduce((acc,event)=>{
            acc[event.location.name]=acc[event.location.name]||Object.assign(event.location, {
                events: []
              });
              acc[event.location.name].events.push(event)
              if(locationNames.indexOf(event.location.name)=== -1) locationNames.push(event.location.name);
              return acc;
        },{})
         locationMarkers = locationNames
            .map(locationName => {
                let location=locationsEvents[locationName]
                let eventsObj;
                if (location) {
                    eventsObj=location.events.map(event=>{
                        const startDateFormatted = moment(event.startTs).format('YYYY-MM-DD HH:mm:ss')
                        const endDateFormatted = moment(event.endTs).format('HH:mm:ss')
                        return <div key={event.name}><a target="blank" href={event.link} >
                        <img src={`${event.imgurl?event.imgurl:'https://i.kinja-img.com/gawker-media/image/upload/Image_Not_Found_1x_qjofp8.png'}`} width="200" height="120"/><br/>
                        {event.name}<br/>
                        {startDateFormatted} - {endDateFormatted}
                        </a>
                        </div>
                    })
                    }
                    let icon = L.icon({
                        iconUrl: tags==='All Category'? getIconUrl(location.events[0].tags[0]):getIconUrl(tags),
                        iconSize: [53, 80],
                        iconAnchor: [27, 78],
                        popupAnchor: [0, -79],
                        // popupAnchor: [0, 0],
                        });
                    // let overSized=eventsObj.length>3
                    // let reducedEventObj=[];
                    // overSized?reducedEventObj.push(eventsObj[0],eventsObj[1],eventsObj[3]):"";
                    return (
                        <Marker icon={icon} key={location.name} position={[location.lat, location.lng]}>
                            <Popup className={`pUp ${ tags==='All Category'? location.events[0].tags[0]:tags}`}>
                            {location.name}<br/>
                            {eventsObj}
                            {/* {console.log("rendered")}
                            {overSized?reducedEventObj:eventsObj}
                            {overSized?<div onClick={()=>reducedEventObj=eventsObj} >Show more!</div>:""} */}
                            </Popup>
                        </Marker>
                    );
                })
            .filter(a => a);
        }           
        let userLocationMarker;
        if (userLocation) {
            userLocationMarker = (
                <CircleMarker key='userlocation' center={[userLocation.lat, userLocation.lng]} radius='5'>
                    <Popup maxWidth='560'>Hey, this is your location.</Popup>
                </CircleMarker>
            );
        }
        // const minZoom=city==='Budapest'? 12 : 13;
        const viewport={
            center: position,
            zoom:window['isDevice']?13: 14,
          }
        return (
            <div className="mapContainer">
                <TopNavBar/>
                <DateTimeSlider/>
                <MapFilters />
                <MapFiltersEventCategory/>
                <MapFiltersEventCity/>
                <ContactPage/>
                <AboutPage/>
                <Map
                    viewport={viewport}
                    attributionControl={false}
                    zoomControl={!window['isDevice']}
                    minZoom={11}
                    maxZoom={18}
                    zoomSnap={0.001}
                    maxBounds={bounds}
                    >
                    <TileLayer subdomains={['mt0','mt1','mt2','mt3']}

                        url="http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}"
                    />
                    {/* <TileLayer
                        // url="http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                        // attribution="&copy; <a href=http://www.openstreetmap.org/copyright>OpenStreetMap</a> contributors"
                        url='https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'
                        attribution= '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
                    /> */}
                    {userLocationMarker}
                    {locationMarkers}
                </Map>
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        position:store.config.position,
        bounds:store.config.bounds,
        events: store.data.events,
        filters: store.config.filters,
        dateFilters: store.data.dateFilters,
        userLocation: store.config.user.location,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EventMap);