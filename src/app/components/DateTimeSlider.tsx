import * as React from "react";
import { connect } from "react-redux";
import Sliderr from "react-rangeslider";
import {
  mapDispatchToProps,
  getEventsForDates,
  getEventsForTags,
  getEventsForTime
} from "../utils";
import { Store, EventFilters, Event } from "../types";
import * as _ from "lodash";
import * as moment from "moment";
moment.locale("hu");

class DateTimeSlider extends React.Component<
  {
    filters: EventFilters;
    dateTimeSliderOpen: boolean;
    setData: any;
    events: Event[];
  },
  {}
> {
  render() {
    const isDevice = window["isDevice"];
    const { filters, setData, events, dateTimeSliderOpen } = this.props;
    const { startTs, dateSlideStartTs, endTs, slider, tags } = filters;
    let filterContents;
    const oneDayOffset = 24 * 3600000;
    const oneHourOffset = 1 * 3600000;
    if (dateTimeSliderOpen) {
      if (endTs - startTs > 31 * 3600000 && endTs !== Infinity) {
        const handleOnChange = value => {
          if (value !== "-") {
            setData(
              "config.filters.dateSlideStartTs",
              startTs + (value>0?value-1:value) * oneDayOffset
            );
            setData("config.filters.slider", value);
          }
        };
        const objss = {};
        let start = startTs;
        let i = 0;
        let eventsForTags;
        if (tags !== "All Category") {
          eventsForTags = getEventsForTags(events, tags);
        } else {
          eventsForTags = events;
        }
        while (start < endTs) {
          if (
            getEventsForDates(eventsForTags || [], start, start + oneDayOffset)
              .length > 0
          ) {
            if (i === 0) {
              objss[i] = `All`;
              objss[i+1] = `${moment(start).format("DD")}`;
            } else {
              objss[i+1] = `${moment(start).format("DD")}`;
              // start += oneDayOffset;
            }
          } else {
            if (i === 0) {
              objss[i] = `All`;
              objss[i+1] = `${moment(start).format("DD")}`;
            } else {
              objss[i+1] = `${moment(start).format("DD")}`;
              // start += oneDayOffset;
            }
            // // objss[i+1] = "-";
            // objss[i+1] = `${moment(start).format("DD")}`;
          }
          i++;
          start += oneDayOffset;
        }
        const formatmin = value =>value===0?"All":moment(dateSlideStartTs).format("YYYY-MM-DD");

        filterContents = (
          <Sliderr
            min={0}
            max={i}
            step={1}
            value={slider}
            labels={objss}
            format={formatmin}
            onChange={handleOnChange}
            orientation={isDevice?"vertical":"horizontal"}
            tooltip={!isDevice}
          />
        );
      } else if (endTs - startTs < 31 * 3600000 && endTs !== Infinity) {
        let now = moment()
          .startOf("hour")
          .valueOf();
        const handleOnChange = value => {
          if (value !== "-") {
            if (startTs < now && now < endTs && value !== 0) {
              setData(
                "config.filters.dateSlideStartTs",
                now + value * oneHourOffset
              );
            } else {
              setData(
                "config.filters.dateSlideStartTs",
                startTs + value * oneHourOffset
              );
            }
            setData("config.filters.slider", value);
          }
        };
        const objss = {};
        let start;
        if (startTs < now && now < endTs) {
          start = now;
        } else {
          start = startTs;
        }
        let i = 0;
        let eventsForTags;
        if (tags !== "All Category") {
          eventsForTags = getEventsForTags(events, tags);
        } else {
          eventsForTags = events;
        }
        while (start < endTs) {
          if (getEventsForTime(eventsForTags, start).length > 0) {
            if (i === 0) {
              objss[i] = `All`;
            } else {
              objss[i] = 
            //   isDevice
            //     ? `${moment(start).format("H")}h`
            //     : 
                `${moment(start).format("H:mm")}`;
            }
          } else {
            if (i === 0) {
              objss[i] = `All`;
            } else {
              // objss[i] = "-";
              objss[i] = `${moment(start).format("H:mm")}`;
            }
          }
          i++;
          start += oneHourOffset;
        }
        const formatmin = value =>value===0?"All" : moment(dateSlideStartTs).format("YYYY-MM-DD H:mm");
        filterContents = (
          <Sliderr
            min={0}
            max={i - 1}
            step={1}
            value={slider}
            labels={objss}
            format={formatmin}
            tooltip={!isDevice}
            onChange={handleOnChange}
            orientation={isDevice?"vertical":"horizontal"}
            // reverse={true}
          />
        );
      }
      return (
        <div>
          <div className={`mapControlDateTimeSlider`}>
            {dateTimeSliderOpen ? filterContents : ""}
          </div>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

function mapStateToProps(store: Store) {
  return {
    filters: store.config.filters,
    dateTimeSliderOpen: store.config.dateTimeSliderOpen,
    events: store.data.events
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateTimeSlider);
