import * as React from 'react';
import { connect } from 'react-redux';
import { Store } from '../../../node_modules/redux';

class Locations extends React.Component<any, {}> {
    render() {
        const locations = this.props.locations.map((location, i) => {
            return <div key={i}>{location.name}, {location.lat}, {location.lng}</div>;
        })
        return (
            <div>
                <h1>Locations</h1>
                {locations}
            </div>
        );
    }
}

const mapStateToProps = (store: Store) => store;

export default connect(mapStateToProps)(Locations);