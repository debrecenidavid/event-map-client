import * as React from 'react';
import { connect } from 'react-redux';

class Loading extends React.Component<any, {}> {
    render() {
        return (
            <div className="loadingContainer">Loading...</div>
        );
    }
}

export default connect()(Loading);