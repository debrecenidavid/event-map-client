import * as React from 'react';
import { connect } from 'react-redux';
import { mapDispatchToProps,} from '../utils';
import {Store} from '../types';

class TopNavBar extends React.Component<any, {}> {
    render() {
        const {config,setData} = this.props;
        const { contactPageOpen,aboutPageOpen } = config;
        return (
            <div className="topnav">
            {/* {window['isDevice']?'':<div/>} */}
            <div/>
            <div className="navMenuLogo">
            {window['isDevice']?'':<img className="todoLogo" src={require('../logo.png')}/>}
            {/* <img className="todoLogo" src={require('../logo.png')}/> */}
            <div className="navMenu">
                <a href="">//TODOHungary</a>
                <a onClick={()=>{
                    setData('config.aboutPageOpen', !aboutPageOpen)
                    setData('config.contactPageOpen', false)
                    setData('config.dateFilterOpen',false)
                    setData('config.categoryFilterOpen',false)
                    setData('config.cityFilterOpen',false)
                }}>About</a>
                <a onClick={()=>{
                    setData('config.contactPageOpen', !contactPageOpen)
                    setData('config.aboutPageOpen',false)
                    setData('config.dateFilterOpen',false)
                    setData('config.categoryFilterOpen',false)
                    setData('config.cityFilterOpen',false)
                }}>Contact</a>
            </div>
            </div>
                <div className='topnavbarLinks'>
                <a href="//www.facebook.com/todohungary/" className="fa fa-facebook"></a>
                <a href="//twitter.com/HungaryTodo" className="fa fa-twitter"></a>
                <a href="//www.instagram.com/todohungary" className="fa fa-instagram"></a>
                </div>
                {/* <input type="text" className="citySearch" placeholder="&#xF002; City.." style={{fontFamily:"Arial, FontAwesome"}} /> */}
                {/* <i className="fa fa-search" style={{color:'white'}}></i>
            <input type="text" className="citySearch" placeholder=" City.." title="Type in a category"></input> */}
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        config:store.config,
        filters: store.config.filters,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TopNavBar);