import * as React from 'react';
import { connect } from 'react-redux';
import * as FontAwesome from 'react-fontawesome';
import { mapDispatchToProps, getEventsForDates,getEventsForTags } from '../utils';
import { Store, EventFilters, DateFilter, Event } from '../types';
import * as _ from 'lodash';

class MapFilters extends React.Component<{cityFilterOpen:boolean,dateTimeSliderOpen:boolean, dateFilterOpen: boolean, categoryFilterOpen:boolean, filters: EventFilters, events: Event[], dateFilters: DateFilter[], setData: any }, {}> {
    render() {
        const isDevice = window['isDevice'];
        const {cityFilterOpen,dateTimeSliderOpen, dateFilterOpen,categoryFilterOpen, events, setData, filters, dateFilters } = this.props;
        let filterContents;
        if (dateFilterOpen) {
            const { startTs, endTs,tags } = filters;
            function makeDateFilterButton(name, isDisabled, [buttonStartTs, buttonEndTs]) {
                const isActive = startTs === buttonStartTs && endTs === buttonEndTs;
                const onClick = isDisabled ? undefined : () => {
                    if (isDevice) {
                        setData('config.dateFilterOpen', false)
                    }
                        setData('config.filters.startTs', buttonStartTs);
                        setData('config.filters.endTs', buttonEndTs);
                        setData('config.filters.dateSlideStartTs', buttonStartTs);
                        setData('config.filters.slider',0);
                    if(buttonEndTs === Infinity){
                        setData('config.dateTimeSliderOpen', false);
                    }
                };
                return (
                    <button
                        key={`${buttonStartTs}-${buttonEndTs}`}
                        className={isActive ? 'button-primary' : isDisabled ? 'disabled' : ''}
                        onClick={onClick}>{name}
                    </button>
                )
            }

            if(tags!=="All Category"){

            }
            const filteredEvents=tags==="All Category"?events:getEventsForTags(events,tags)
            const uniqOptions = _.uniqBy(dateFilters, item => `${item.startTs}-${item.endTs}`)
                .map(opt => {
                    const hasEvents = getEventsForDates(filteredEvents || [], opt.startTs, opt.endTs).length;
                    return Object.assign(opt, { hasEvents })
                });

            filterContents = (
                <div className="controller">
                    {/* <h6 className="title">Dates</h6> */}
                    <img src={require('../logo.png')} width="100" height="108"/>
                    <div className="vertical">
                        {uniqOptions.map(o => makeDateFilterButton(o.name, !o.hasEvents, [o.startTs, o.endTs]))}
                    </div>
                    <br/>
                    <div className="vertical">
                    <button
                        className={dateTimeSliderOpen ? 'button-primary' : endTs===Infinity ? 'disabled' : ''}
                        onClick={endTs===Infinity?undefined:() => {
                            setData('config.dateTimeSliderOpen', !dateTimeSliderOpen);
                        }}>TimeLine {dateTimeSliderOpen?"ON":"OFF"}
                    </button>
                    </div>
                    <pre>
                        <code>
                            Credits: <a href="mailto:todohungary@gmail.com">//TODOHungary</a>
                        </code>
                    </pre>
                </div>
            )
        }
        const filterState = dateFilterOpen ? 'open' : 'closed';
        const filterContainer = dateFilterOpen
            ? (<div>
                <div className="toggleIcon" onClick={() => {
                    setData('config.dateFilterOpen', !dateFilterOpen);
                }}>
                    <FontAwesome name='close' />
                </div>
                {filterContents}
            </div>)
            : (<div className="toggleIcon" onClick={() => {
                setData('config.dateFilterOpen', !dateFilterOpen);
                setData('config.categoryFilterOpen', false);
                setData('config.cityFilterOpen',false);
                setData('config.aboutPageOpen',false);
                setData('config.contactPageOpen',false);
            }}>
                <FontAwesome name='calendar' />
            </div>);

           const dateIcon=dateFilterOpen && !isDevice?
           (<div className="dateIcon" onClick={() => {
            setData('config.dateFilterOpen', !dateFilterOpen);
            setData('config.categoryFilterOpen', false);
            setData('config.cityFilterOpen',false);
            setData('config.aboutPageOpen',false);
            setData('config.contactPageOpen',false);
            }}>
            <FontAwesome name='calendar'/>
            </div>):(<div></div>)
            if(categoryFilterOpen && isDevice || cityFilterOpen && isDevice) return(<div></div>)//TODO any other filter open until method
        return (
            <div onClick={() => {
                isDevice?
                setData('config.dateFilterOpen', !dateFilterOpen):"";
            }}>
                {dateIcon}
                <div className={`mapControl ${filterState}`}>
                    {filterContainer}
                </div>
            </div>
        );
    }
}

function mapStateToProps(store: Store) {
    return {
        categoryFilterOpen:store.config.categoryFilterOpen,
        dateFilterOpen: store.config.dateFilterOpen,
        cityFilterOpen:store.config.cityFilterOpen,
        dateTimeSliderOpen:store.config.dateTimeSliderOpen,
        filters: store.config.filters,
        dateFilters: store.data.dateFilters,
        events: store.data.events,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapFilters);