module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [{
        watch: true,
        name: "event-client-server",
        script: "./server/server.js",
        env_live: {
            NODE_ENV: "live"
        }
    }]
};
